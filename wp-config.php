<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ecta' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j-~uubXs~jw_%tsM~~iF&D[>>~y`XJr*:z`.)r,|:H;vz#qwPbGt`_ma5Cm&=8Ne' );
define( 'SECURE_AUTH_KEY',  '<xW9&C@-1kG=>0y2zU#9_KQJJ^Oa_tGD_-5B>$|gE}jodJF8jb(~I6FT^{z&,p1#' );
define( 'LOGGED_IN_KEY',    '9i2]_piqFe)ee1=~Ay.ab`o(zXbpRdI{ )$`A6 5=T&Y@jA1v+BBYF{K?8n+fY, ' );
define( 'NONCE_KEY',        'tzNQbzP&qW3h4-_CthbmgZ@_3a%%M+%C^~M%H oVxY}e5t=DG*#MB@CuP_Ag_zd:' );
define( 'AUTH_SALT',        '=2&&5lmBEy3!!*uq_{x0COO:dCIvom*4O{WODtH3mD:oX1`=xi5CYiWgnBj7rINC' );
define( 'SECURE_AUTH_SALT', 'aT `_*0Liq!;=G4@+BmI1 6))Pq(,#*Z5?GXDpOu5[rA`YQyZ{_el&Vx=GTG>*~{' );
define( 'LOGGED_IN_SALT',   'J>w4<~Zh=~{Y#|;sFp&5Tjgk}>c$]8pQfB,8F)v#eE;#1nKd9/n9%6_~yTA|3wb(' );
define( 'NONCE_SALT',       '<mHH/If]_QA:%$5C.A,})`nvvj [fqcw;/zU8wA%]Xf]ZLHo*c7NoOs02.qj)nv8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
